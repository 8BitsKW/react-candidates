import React from "react"
import ReactDOM from "react-dom"
import "./index.css"

const App = () => (
    <div className="w-full h-full flex justify-center items-center">
        <h1>Hello There!</h1>
    </div>
)

ReactDOM.render(<App />, document.getElementById("root"))
